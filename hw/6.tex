\input{include/header.tex}
\input{include/theories/iplc.tex}

\title{Assignment 6}
\subtitle{Proofs by induction}

\begin{document}

\newcommand{\zero}{\texttt{0}}
\newcommand{\suc}{\texttt{S}}

\maketitle

\section*{Grading}

  Each exercise in this assignment is worth 10 points, for a total of 30 points.

\section*{Definitions}

  This is a review of the definitions we've seen in recent lectures, cleaned up into a format that's hopefully a little easier to reference. You should still review the lecture videos too; as usual, it's more effective to learn this stuff by studying the process than by studying the end result!

  The goal here is to define natural numbers and addition \insist{from scratch} (or as close to ``scratch'' as realistically possible), so that we can practice proving things about the set of natural numbers. Proving things about natural numbers is a good way to begin studying the proof technique of \vocabstyle{induction}, which is a major topic in modern proof theory and program verification.

  We assume some basic set theory, but we assume \insist{nothing} about numbers. We avoid circular definitions (where A is defined in terms of B and B is defined in terms of A), but we allow a limited form of recursive definition that can be reasoned about in terms of basic set theory.

  \subsection*{Natural numbers}

    First, we define the set of natural numbers, which is traditionally written $\mathbb{N}$ and sometimes pronounced ``nat''. This definition involves two \vocabstyle{operators}, written \zero\ and \suc. The set $\mathbb{N}$ is defined recursively by these two statements:

    \begin{tabular}{r r c l}
      & $\zero$ & $\in \mathbb{N}$ & ``zero is a natural number'' \\
      $\forall (n \in \mathbb{N}).$ & $\suc(n)$ & $\in \mathbb{N}$ & ``if $n$ is a natural number, then $\suc(n)$ is a natural number''
    \end{tabular}

    It's important to understand that this is all \vocabstyle{syntax}: the $\suc$ operator doesn't really ``do'' anything, in the same way that numbers like 0 or 5 or 100 don't really ``do'' anything. Syntax is just \insist{data}: $\suc(n)$ is a \insist{different piece of data} than $n$, but the \insist{same kind of data} as $n$ (still a natural number). The letter $\suc$ stands for ``successor'': $\suc(n)$ is traditionally pronounced ``the successor of $n$''.

    When we \insist{read} this syntax, we can \insist{imagine} that the $\suc$ operator corresponds to an ``increment'' operation: we might \insist{read} $\suc(n)$ as ``the result of incrementing (adding one to) $n$''. Keep in mind that this is ultimately just an \insist{intuition} that we use to \insist{think} about this definition of natural numbers, and ``proof by intuition'' is generally not a valid proof technique.

    Other common notations for numbers can be understood as \vocabstyle{shorthand} for this kind of data. For example, 1 is shorthand for $\suc(\zero)$, 2 is shorthand for $\suc(\suc(\zero))$, and 5 is shorthand for $\suc(\suc(\suc(\suc(\suc(\zero)))))$.

    This definition of $\mathbb{N}$ is very minimalistic: it deliberately avoids mentioning anything like addition or a pre-existing set of integers. This helps us avoid circular definitions, and also helps us justify the proof technique of natural induction.

  \subsection*{Natural induction}

    Natural induction is our main proof technique for proving statements of the form $\forall(n \in \mathbb{N}).\ P(n)$, for some \vocabstyle{predicate} $P$.

    There are lots of properties that are true of all natural numbers. For example, with suitable definitions of our standard arithmetic operators, we should be able to prove a statement of this form when $P(x) \defequals (x \ge \zero)$, or when $P(x) \defequals (x + x = 2 \times x)$.

    A natural induction proof requires two subproofs:
    \begin{itemize}[noitemsep]
      \item \vocabstyle{base case}: $P(\zero)$
      \item \vocabstyle{inductive case} (or \vocabstyle{step case}): $\forall(x \in \mathbb{N}).\ P(x) \to P(\suc(x))$
    \end{itemize}

    As we've seen with the $\implIntro$ rule in formal proofs, to prove an implication, you assume the left side and prove the right side. In the inductive case this means you get an assumption of $P(x)$ \insist{and no other assumptions about $x$}, and you have to prove $P(\suc(x))$. The assumption of $P(x)$ is traditionally called the \vocabstyle{inductive hypothesis}.

    If this sounds like a recursive function, that's because it essentially is: a natural induction proof is a recursive algorithm that builds a proof for any natural number that it's given as input. If you put in $\suc(\suc(\suc(\zero)))$, the algorithm has all of these proofs to work with:

    \begin{tabular}{l c l l}
      & & $P(\zero)$ & provided by your base case \\
      $P(\zero)$ & $\to$ & $P(\suc(\zero))$ & provided by your inductive case, $x \defequals \zero$ \\
      $P(\suc(\zero))$ & $\to$ & $P(\suc(\suc(\zero)))$ & provided by your inductive case, $x \defequals \suc(\zero)$ \\
      $P(\suc(\suc(\zero)))$ & $\to$ & $P(\suc(\suc(\suc(\zero))))$ & provided by your inductive case, $x \defequals \suc(\suc(\zero))$ \\
    \end{tabular}

    It just takes three uses of the $\implIntro$ rule to combine these into a proof of $P(\suc(\suc(\suc(\zero))))$, which requires no additional assumptions. Our definition of the set of natural numbers guarantees that this process will work for \insist{any} natural number, and that's why we consider a proof by natural induction to be valid as a proof for \insist{all} natural numbers.

    There is a somewhat more powerful form of natural induction called \vocabstyle{strong induction}, which we aren't going to cover in this class; the form of natural induction defined above is sometimes called \vocabstyle{weak induction} to be precise. Strong induction can be proven by weak induction, so we typically define weak induction first and then derive strong induction as a lemma.

  \subsection*{Addition}

    We define addition with two equations. We don't assume \insist{anything else} about addition: we can prove all the rest of it from these two defining equations.

    \begin{tabular}{l c c c c c}
      $\forall (n \in \mathbb{N}).$ & $\zero$ & $+$ & $n$ & $=$ & $n$ \\
      $\forall (m, n \in \mathbb{N}).$ & $\suc(m)$ & $+$ & $n$ & $=$ & $\suc(m + n)$
    \end{tabular}

  \subsection*{Examples}

  Here's a proof that 2+3=5:

  \begin{tabular}{c r r c l r c l l}
    & & $\suc(\suc($ & $\zero$ & $))$ & $+$ & $\suc(\suc(\suc(\zero)))$ & & \\
    $=$ & $\suc($ & $\suc($ & $\zero$ & $)$ & $+$ & $\suc(\suc(\suc(\zero)))$ & $)$ & by second addition rule \\
    $=$ & $\suc(\suc($ & & $\zero$ & & $+$ & $\suc(\suc(\suc(\zero)))$ & $))$ & by second addition rule \\
    $=$ & $\suc(\suc($ & & & & & $\suc(\suc(\suc(\zero)))$ & $))$ & by first addition rule
  \end{tabular}

  Here's the first inductive proof we saw in lecture, written in a minimal style that's acceptable for the exercises on this assignment. Again, \insist{studying this finished product by itself will not be very helpful}: make sure to review the lecture content covering the \insist{process} of constructing proofs by induction.

    Goal: prove $\forall (x \in \mathbb{N}).\ x + \zero = x$

    \begin{itemize}
      \item Base case: $\zero + \zero = \zero$

        Proof: first addition rule, where $n \defequals 0$

      \item Step case: $(x + \zero = x) \to (\suc(x) + \zero = \suc(x))$

        Proof:

        \begin{tabular}{c l l}
          & $\suc(x) + \zero$ & \\
          $=$ & $\suc(x + \zero)$ & second addition rule, where $m \defequals x$ and $n \defequals \zero$ \\
          $=$ & $\suc(x)$ & inductive hypothesis
        \end{tabular}
    \end{itemize}

\section{Exercises}

Prove each of these statements using \insist{only} the definitions provided above (including the example proof, which you can use as a lemma). Only exercise 3 requires natural induction.

\begin{enumerate}[noitemsep]
  \item $\forall (x \in \mathbb{N}).\ \suc(\suc(\suc(\suc(\zero)))) + \suc(\suc(\zero)) = \suc(\suc(\suc(\suc(\suc(\suc(\zero))))))$
  \item $\forall (x \in \mathbb{N}).\ \suc(\zero) + x = \suc(x)$
  \item $\forall (x \in \mathbb{N}).\ x + \suc(\zero) = \suc(x)$
\end{enumerate}

\end{document}
