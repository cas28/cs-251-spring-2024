\input{include/header.tex}
\input{include/theories/bool.tex}
\input{include/theories/iplc.tex}

\input{notes/3.glt}

\indextitle{Intro to natural deduction}
\subtitle{The structure of proofs}

\makeglossaries


\begin{document}

\newpage

\maketitle
\tableofcontents

\newpage


\section{Introduction}

  In the course material so far, we have seen two different major \vocabs{proof-technique} for proving \vocabs{tautology} in \vocab{Boolean-logic}: \vocabs{truth-table} and \vocab{equational-reasoning}.

  We have also spent some time discussing \insist{why} we have more than one proof technique for the same system of logic: each individual proof technique has strengths and weaknesses, kind of similar to how each individual programming language has strengths and weaknesses.

  \begin{itemize}
    \item Truth tables are a kind of ``brute force'' proof technique: the calculation process is very straightforward, but it takes exponentially more work as the number of variables increases.

    \item Equational reasoning is a somewhat more ``intuitive'' proof technique: it often requires some amount of trial and error, but it scales relatively well as the number of variables increases.
  \end{itemize}

  In these notes we will introduce a third proof technique known as \vocab{natural-deduction}, which is one of the core concepts in the field that we call \vocab{proof-theory}. In particular, we will introduce a system of natural deduction for proving things about Boolean expressions.


\section{Definitions}

  \subsection{Rules of inference}

    A proof system is a kind of puzzle game, and \vocabs{rule-of-inference} (or \vocabstyle{inference rules}) are the ``moves'' that a player is allowed to make in the game. A proof system is \insist{defined} by which rules of inference it allows.

    A rule of inference is traditionally written in a two-dimensional notation: a horizontal line between two rows of symbols, with a name next to one of the endpoints of the line. (It looks kind of like a fraction, but it's not a fraction.) The general structure looks like this:

    \begin{center}
      \AxiomC{$\vocabs{premise}$}
      \LeftLabel{$\rulename{RuleName}$}
      \UnaryInfC{$\vocab{conclusion}$}
      \DisplayProof
    \end{center}

    It doesn't matter whether we write the rule name on the left or right of the line:\\
      \AxiomC{$\vocabs{premise}$}
      \LeftLabel{$\rulename{RuleName}$}
      \UnaryInfC{$\vocab{conclusion}$}
      \DisplayProof
    and
      \AxiomC{$\vocabs{premise}$}
      \RightLabel{$\rulename{RuleName}$}
      \UnaryInfC{$\vocab{conclusion}$}
      \DisplayProof
    have the same meaning.

    Underneath the line, there is a \insist{single} \vocab{expression} called the \vocab{conclusion}. On top of the line, there may be \insist{any natural number of} \vocabs{expression} called the \vocabs{premise} (including zero). When an inference rule has no premises, we sometimes call it an \vocab{axiom}. When describing an inference rule, we say the conclusion ``follows from'' the premises.

    Here is what the structures of zero-, one-, two-, and three-premise inference rules look like:

    \begin{center}
      \AxiomC{}
      \LeftLabel{$\rulename{RuleName}$}
      \UnaryInfC{$\vocab{conclusion}$}
      \DisplayProof

      \AxiomC{$\vocab{premise}_1$}
      \LeftLabel{$\rulename{RuleName}$}
      \UnaryInfC{$\vocab{conclusion}$}
      \DisplayProof

      \AxiomC{$\vocab{premise}_1$}
      \AxiomC{$\vocab{premise}_2$}
      \LeftLabel{$\rulename{RuleName}$}
      \BinaryInfC{$\vocab{conclusion}$}
      \DisplayProof

      \AxiomC{$\vocab{premise}_1$}
      \AxiomC{$\vocab{premise}_2$}
      \AxiomC{$\vocab{premise}_3$}
      \LeftLabel{$\rulename{RuleName}$}
      \TrinaryInfC{$\vocab{conclusion}$}
      \DisplayProof
    \end{center}

    When writing proofs, we can read inference rules in two ways:
    \begin{itemize}
      \item Top-to-bottom: if we \insist{have} proofs for the premises of an inference rule, then we can use the rule to \vocab{construct} a proof of the conclusion.
      \item Bottom-to-top: if we \insist{want to} prove the conclusion of an inference rule, then we can use the rule to \vocab{split} (or ``deconstruct'') our problem into subproblems: now we have to construct a proof of each premise.
    \end{itemize}

    The premises and conclusion in an inference rule are \vocabs{expression}. Here is an example that we'll revisit later in these notes:

    \begin{center}
      \AndElimL
    \end{center}

    From top to bottom, the \andElimL\ rule says that if we have a proof of $a \conj b$, then we can construct a proof of $a$. From bottom to top, the \andElimL\ rule says that if we want to prove $a$, we can do it by constructing a proof of $a \conj b$.


  \subsection{Proof trees}

    A \vocab{proof-tree} is a particular kind of tree structure that we construct by following rules of inference. This is where \vocab{unification} comes into play, and where we get our first formal definition of the word \vocab{proof}.
    
    Proof trees look a little different than traditional tree diagrams in computer science: for one thing, a proof tree ``grows'' \insist{up} with its root at the \insist{bottom}, like most actual trees in nature. A proof tree is made of multiple inference rules piled on top of each other in a branching structure.

    When we start constructing a proof tree, we start with some \vocab{expression} that we want to prove and some \vocab{context} that we want to prove it in. A \vocab{context} is a \insist{list} of expressions.

    At each step of constructing the tree, we must choose an inference rule whose \vocab{conclusion} \vocabs{unify-with} the expression that we're trying to prove. There may be more than one usable rule to choose from. It \insist{is} possible to make a ``wrong'' choice, so we may sometimes try a rule and then ``backtrack'' if it doesn't work out.

    When we choose a rule, we take the \vocab{unifying-substitution} that we got from unifying the rule's \vocab{conclusion} with our expression: call it $\sigma$. To construct each next \vocab{branch} of our proof tree, we \vocab{substitute} with $\sigma$ in each \vocab{premise} of the inference rule (if there are any).

    For example:

    \longexample{
      goal: $P, Q \entails P \conj Q$

      \medskip

      rule: \AndIntro

      \medskip

      unification problem: $P \conj Q \subsumedby a \conj b$

      \medskip

      unifying substitution: $\sigma \defequals \left[ \begin{tabular}{c c c}
        $\Gamma$ & $\defequals$ & $P, Q$\\
        $a$ & $\defequals$ & $P$\\
        $b$ & $\defequals$ & $Q$
      \end{tabular}\right]$

      \medskip

      proof tree:

        \AxiomC{$\sigma\left(\Gamma \entails a\right)$}
        \AxiomC{$\sigma\left(\Gamma \entails b\right)$}
        \LeftLabel{\andIntro}
        \BinaryInfC{$\sigma\left(\Gamma \entails a \conj b\right)$}
        \DisplayProof
        \quad
        which is
        \quad
        \AxiomC{$P, Q \entails P$}
        \AxiomC{$P, Q \entails Q$}
        \LeftLabel{\andIntro}
        \BinaryInfC{$P, Q \entails P \conj Q$}
        \DisplayProof
    }

    We repeat this process for each expression in our proof tree that does not have a line over it. We can do this in any order, and there is no ``wrong'' order. This is what gives rise to the tree structure of our proof tree. For example:

    \longexample{
      goal: $P, Q, R, S \entails \left(P \conj Q\right) \conj \left(R \conj S\right)$

      \medskip

      rule: \AndIntro

      \medskip

      unification problem: $P, Q, R, S \entails \left(P \conj Q\right) \conj \left(R \conj S\right) \subsumedby \Gamma \entails a \conj b$

      \medskip

      unifying substitution: $\sigma \defequals \left[ \begin{tabular}{c c c}
        $\Gamma$ & $\defequals$ & $P, Q, R, S$\\
        $a$ & $\defequals$ & $P \conj Q$\\
        $b$ & $\defequals$ & $R \conj S$
      \end{tabular}\right]$

      \medskip

      proof tree:
          \AxiomC{$P, Q, R, S \entails P \conj Q$}
          \AxiomC{$P, Q, R, S \entails R \conj S$}
        \LeftLabel{\andIntro}
        \BinaryInfC{$P, Q, R, S \entails \left(P \conj Q\right) \conj \left(R \conj S\right)$}
        \DisplayProof

      \medskip

      rule: \andIntro

      \medskip

      unification problem: $P, Q, R, S \entails P \conj Q \subsumedby \Gamma \entails a \conj b$

      \medskip

      unifying substitution: $\sigma \defequals \left[ \begin{tabular}{c c c}
        $\Gamma$ & $\defequals$ & $P, Q, R, S$\\
        $a$ & $\defequals$ & $P$\\
        $b$ & $\defequals$ & $Q$
      \end{tabular}\right]$

      \medskip

      proof tree:
            \AxiomC{$P, Q, R, S \entails P$}
            \AxiomC{$P, Q, R, S \entails Q$}
          \LeftLabel{\andIntro}
          \BinaryInfC{$P, Q, R, S \entails P \conj Q$}
          \AxiomC{$P, Q, R, S \entails R \conj S$}
        \LeftLabel{\andIntro}
        \BinaryInfC{$P, Q, R, S \entails \left(P \conj Q\right) \conj \left(R \conj S\right)$}
        \DisplayProof

      \medskip

      rule: \andIntro

      \medskip

      unification problem: $P, Q, R, S \entails P \conj Q \subsumedby \Gamma \entails a \conj b$

      \medskip

      unifying substitution: $\sigma \defequals \left[ \begin{tabular}{c c c}
        $\Gamma$ & $\defequals$ & $P, Q, R, S$\\
        $a$ & $\defequals$ & $R$\\
        $b$ & $\defequals$ & $S$
      \end{tabular}\right]$

      \medskip

      proof tree:
            \AxiomC{$P, Q, R, S \entails P$}
            \AxiomC{$P, Q, R, S \entails Q$}
          \LeftLabel{\andIntro}
          \BinaryInfC{$P, Q, R, S \entails P \conj Q$}
            \AxiomC{$P, Q, R, S \entails R$}
            \AxiomC{$P, Q, R, S \entails S$}
          \LeftLabel{\andIntro}
          \BinaryInfC{$P, Q, R, S \entails R \conj S$}
        \LeftLabel{\andIntro}
        \BinaryInfC{$P, Q, R, S \entails \left(P \conj Q\right) \conj \left(R \conj S\right)$}
        \DisplayProof
    }


  \subsection{Errors}

    What can go wrong when we try to construct a \vocab{proof-tree}?

    If we choose an invalid \vocablink{rule-of-inference}{inference rule} at any step, our \vocab{unification-problem} will not have a \vocab{unifying-substitution}. Let's start in the middle of constructing that last proof tree:

    \longexample{
      proof tree:
            \AxiomC{$P, Q, R, S \entails P$}
            \AxiomC{$P, Q, R, S \entails Q$}
          \LeftLabel{\andIntro}
          \BinaryInfC{$P, Q, R, S \entails P \conj Q$}
          \AxiomC{$P, Q, R, S \entails R \conj S$}
        \LeftLabel{\andIntro}
        \BinaryInfC{$P, Q, R, S \entails \left(P \conj Q\right) \conj \left(R \conj S\right)$}
        \DisplayProof
    }

    We've seen that we can use \andIntro\ on the $R \conj S$ premise. What if we tried to use a different rule?

    Let's consider this rule as a potential option:

    \longexample{
      rule: \OrIntroL

      \medskip

      unification problem: $P, Q, R, S \entails R \conj S \subsumedby \Gamma \entails a \vee b$

      \medskip

      unifying substitution: \textcolor{red}{none}
    }

    This means that the \orIntroL\ rule is \insist{invalid} to use in this part of this proof tree.

    To \vocab{check} a proof, we just work through the unification problems that are represented by the rules in the tree, checking that the premises match the conclusion according to the definition of the rule being used.

    
  \subsection{Complete proofs}

    A proof tree is \vocablink{complete-proof-tree}{complete} when every expression in the tree has a line above it. For example, the \topIntro\ rule allows us to put a line over $\top$ if it is the only symbol in a expression:


    \longexample{
      rules: \AndIntro \quad \TopIntro

      expression: $\left(\top \conj \top\right) \conj \left(\top \conj \top\right)$

      proof tree:
              \AxiomC{}
            \LeftLabel{\topIntro}
            \UnaryInfC{$\entails \top$}
              \AxiomC{}
            \RightLabel{\topIntro}
            \UnaryInfC{$\entails\top$}
          \LeftLabel{\andIntro}
          \BinaryInfC{$\entails\top \conj \top$}
              \AxiomC{}
            \LeftLabel{\topIntro}
            \UnaryInfC{$\entails\top$}
              \AxiomC{}
            \RightLabel{\topIntro}
            \UnaryInfC{$\entails\top$}
          \RightLabel{\andIntro}
          \BinaryInfC{$\entails\top \conj \top$}
        \LeftLabel{\andIntro}
        \BinaryInfC{$\entails\left(\top \conj \top\right) \conj \left(\top \conj \top\right)$}
        \DisplayProof
    }

    When we have a \vocab{complete-proof-tree} for some expression, we say we have a \vocab{proof} of the expression.

    In most proof systems, it is possible for a single expression to have multiple different proofs.


  \subsection{Assumptions}

    When we write something like $P, Q, R, S \entails P \conj Q$, the \vocab{context} of the $\entails$ symbol is a list of \vocabs{assumption}. Conceptually, an assumption represents a kind of ``placeholder'' in a proof: when we prove a statement with assumptions, we are proving that our conclusion is true \insist{if} all of the assumptions are provable.

    In a more computational sense, an assumption can be thought of as a \vocab{variable} in a proof. Like a variables, an assumption has a \vocab{scope}, which is the data represented by a \vocab{context}.

    The system of natural deduction has a \insist{special} rule for working with assumptions, which is somewhat different than all of the other rules in the system:

    \begin{center}
      \Variable
    \end{center}

    The $\variable$ rule can be used to prove some expression \insist{if and only if} that expression is listed \insist{identically} in the context of assumptions.

    For example, it is valid to use the $\variable$ rule to prove $P, Q \entails P$, but not to prove $P, Q \entails R$. It is also invalid to use the $\variable$ rule to prove $P \conj Q \entails Q$, because the expression $Q$ is not one of the elements of the context, is is a \insist{subexpression} of one of the elements of the context.

    With the $\variable$ rule, we can complete our example from earlier:

    \begin{center}
            \AxiomC{}
          \LeftLabel{\variable}
          \UnaryInfC{$P, Q, R, S \entails P$}
            \AxiomC{}
          \RightLabel{\variable}
          \UnaryInfC{$P, Q, R, S \entails Q$}
        \LeftLabel{\andIntro}
        \BinaryInfC{$P, Q, R, S \entails P \conj Q$}
            \AxiomC{}
          \LeftLabel{\variable}
          \UnaryInfC{$P, Q, R, S \entails R$}
            \AxiomC{}
          \RightLabel{\variable}
          \UnaryInfC{$P, Q, R, S \entails S$}
        \RightLabel{\andIntro}
        \BinaryInfC{$P, Q, R, S \entails R \conj S$}
      \LeftLabel{\andIntro}
      \BinaryInfC{$P, Q, R, S \entails \left(P \conj Q\right) \conj \left(R \conj S\right)$}
      \DisplayProof
    \end{center}


\section{Rules}

  Here are all of the \vocabs{rule-of-inference} in our proof system of \vocab{natural-deduction} for \vocab{Boolean-logic}.

  \begin{center}
    \Variable

    \AndIntro \quad \AndElim

    \OrIntroL \quad \OrIntroR

    \OrElim

    \ImplIntro \quad \ImplElim

    \TopIntro \quad \BotElim
    
    \NotIntro \quad \NotElim

    \LEM
  \end{center}

  These rule names use a traditional naming convention:

  \begin{itemize}
    \item \rulename{Intro} is short for \vocab{introduction-rule}, a rule which has its respective operator in its \vocab{conclusion} and not in its \vocabs{premise}.
    \item \rulename{Elim} is short for \vocab{elimination-rule}, a rule which has its respective operator in one of its \vocabs{premise} and not in its \vocab{conclusion}.
    \item The $_L$ and $_R$ subscripts are short for ``left'' and ``right''.
  \end{itemize}

  When we're writing a \vocab{proof-tree} in a particular \vocab{proof-system}, we must \insist{only} use the \vocabs{inference-rule} provided by the proof system. Making up new inference rules can be a productive form of exploration and research, but it means you're working in a fundamentally different proof system than you started with, and therefore any proof you construct might not be valid in the original proof system.

  We will discuss these rules and plenty of example proofs in lecture!


\input{include/footer.tex}
\end{document}

