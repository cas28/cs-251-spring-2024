\input{include/header.tex}
\input{include/theories/bool.tex}
\input{include/theories/ht.tex}
\input{include/theories/verilog.tex}

% auto-generated glossary terms
\input{notes/1.glt}

\indextitle{Truth tables and lookup tables}
\subtitle{Computing with finite sets}

\makeglossaries


\begin{document}

\maketitle
\tableofcontents

\newpage



\section{Introduction}

  In our first couple lectures, we have been reviewing the concept of a \vocab{truth-table}, which we use both for \insist{defining} logical operators and for \insist{calculating} the values of logical expressions.

  You are almost certainly familiar with truth tables from your previous studies, but it is important to note that a \vocab{truth-table} is actually a \insist{special case} of a more general concept known as a \vocab{lookup-table}. Lookup tables are a critical concept in the study of computation, both in hardware and in software.

  In these notes, we will explore how three \insist{different} systems of logic can be defined in terms of lookup tables. We will also discuss why we need multiple different systems of logic in our toolkit of techniques for reasoning about computation.



\section{Boolean logic}

  \vocab{Boolean-logic} is the system of logic that you are most likely already familiar with.

  \subsection{Truth values}

    In Boolean logic, there are exactly two \vocabs{truth-value}, which we traditionally call ``true'' ($\true$) and ``false'' ($\false$).

  \subsection{Operators}

    We traditionally \insist{define} the operators of Boolean logic with \vocabs{truth-table}. Here are some examples that should look familiar:

    \begin{center}
      \NEG \quad \CONJ \quad \DISJ \quad \IMPL
    \end{center}

    In order for a truth table to be complete, it must define a valid ``output'' for \insist{every possible combination} of ``inputs''. For example, this truth table below is \insist{incomplete} because it does not specify the value of $\true \xdisj \false$ (which should be $\true$ according to the standard Boolean definition of the $\xdisj$ operator).

    \begin{center}
      \begin{tabular}{c | c ? c}
        $P$ & $Q$ & $P \xdisj Q$ \\
        \thickhline
        \true & \true & \false \\
        \hline
        \false & \true & \true \\
        \hline
        \false & \false & \false \\
      \end{tabular}
    \end{center}

  \subsection{Calculation}

    We also traditionally \insist{calculate} the value of a Boolean logic expression with a truth table. The mechanical process goes like this:

    \begin{enumerate}
      \item Identify every unique subexpression of the expression, and list them in order from smallest to largest. For example, the subexpressions of $\neg (P \disj \neg Q) \impl P$ from smallest to largest are $P$, $Q$, $\neg Q$, $P \disj \neg Q$, $\neg (P \disj \neg Q)$, and finally the whole expression $\neg (P \disj \neg Q) \impl P$.
      \item \insist{Calculate} the value of each subexpression in order, using the truth tables that \insist{define} each operator. For example, $\neg Q$ is calculated by the truth table for the $\neg$ operator using the $Q$ column as input, and then $P \disj \neg Q$ is calculated by the truth table for the $\disj$ operator using the $P$ and $\neg Q$ columns as input.
    \end{enumerate}

    Here is the whole explicit calculation for the value of $\neg (P \disj \neg Q) \impl P$ in Boolean logic:

    \begin{center}
      \begin{tabular}{c | c ? c ? c ? c ? c}
        $P$ & $Q$ & $\neg Q$ & $P \disj \neg Q$ & $\neg (P \disj \neg Q)$ & $\neg (P \disj \neg Q) \impl P$ \\
        \thickhline
        \true & \true & \false & \true & \false & \true \\
        \hline
        \true & \false & \true & \true & \false & \true \\
        \hline
        \false & \true & \false & \false & \true & \false \\
        \hline
        \false & \false & \true & \true & \false & \true \\
      \end{tabular}
    \end{center}


\section{Tables as mathematical objects}

  Before we explore a couple other systems of logic, let's take a moment to explore the concept of a ``table'' from a mathematical perspective.

  \subsection{As matrices}

    A truth table with $n$ inputs and one output can be seen as an $n$-dimensional \vocab{matrix}.

    The ``length'' of each dimension is the number of distinct truth values in the system of logic that we're working with. For Boolean logic, this means a two-input truth table is a 2x2 matrix, a three-input truth table is a 2x2x2 matrix, etc.

    For example, we might represent the Boolean definition of the $\disj$ operator with the following notation:

    \begin{center}
      \DISJ
      \quad can be written as \quad
      \begin{tabular}{c c ? c | c}
        \multicolumn{2}{c}{} & \multicolumn{2}{c}{$Q$} \\
        & $P \disj Q$ & \true & \false \\
        \thickcline{2-4}
        \multirow{2}{*}{$P$} & \true & \true & \true \\
        \cline{2-4}
        & \false & \true & \false \\
      \end{tabular}
    \end{center}

    This is just a different way to write the same information, but in systems of logic with more than two truth values, this notation can often be easier to read when defining two-input operators. It also sometimes reveals patterns that are harder to recognize in the other notation.

    More abstractly, the interpretation of truth tables as matrices connects logic to linear algebra.

    \begin{center}
      \begin{tabular}{c c ? c | c}
        \multicolumn{2}{c}{} & \multicolumn{2}{c}{$Q$} \\
        & $P \disj Q$ & \true & \false \\
        \thickcline{2-4}
        \multirow{2}{*}{$P$} & \true & \true & \true \\
        \cline{2-4}
        & \false & \true & \false \\
      \end{tabular}
      \quad can be represented by the mathematical matrix \quad
      $\begin{bmatrix}
        1 & 1 \\
        1 & 0
      \end{bmatrix}$
    \end{center}

    This enables us to apply some of the techniques of linear algebra in the study of logic, although we won't see them in this course.

  \subsection{As functions}

    A \vocab{truth-table} with $n$ inputs and one output can be seen as an $n$-argument \vocab{function}.

    The ``argument types'' and ``return type'' in these functions are all the same: they are all \vocabs{truth-value} in whichever system of logic we are using. In Boolean logic, they are all \texttt{bool} data values.

    More generally, a \vocab{lookup-table} is a function defined by \insist{listing the output for every possible input}. Any function whose arguments all have \vocabs{finite-type} can be defined as a lookup table.

    For example, consider a function which multiplies two arguments, where each argument is an element of the set \{-1, 0, 1\}. We can define this function with the following \vocab{lookup-table}:

    \begin{center}
      \begin{tabular}{c c ? c | c | c}
        \multicolumn{2}{c}{} & \multicolumn{3}{c}{$y$} \\
        & $x * y$ & -1 & 0 & 1 \\
        \thickcline{2-5}
        \multirow{3}{*}{$x$}
        & -1 & 1 & 0 & -1 \\
        \cline{2-5}
        & 0 & 0 & 0 & 0 \\
        \cline{2-5}
        & 1 & -1 & 0 & 1 \\
      \end{tabular}
    \end{center}

    The traditional \texttt{switch}/\texttt{case} notation in many programming languages is a form of lookup table, although it usually does not require that the programmer defines a \insist{complete} lookup table.


\section{Intuitionistic logic}

  Another system of logic that we can define with \vocabs{lookup-table} is \vocab{intuitionistic-logic}. To be precise, there are many related systems of ``intuitionistic logic''; the form of intuitionistic logic that we will discuss in this section is sometimes known as \vocab{HT-logic} (or the ``logic of \textbf{h}ere and \textbf{t}here'').

  Broadly, intuitionistic logic rejects the Boolean claim that ``everything must be either true or false''. This is evident in the fact that $P \disj \neg P$ is a \vocab{tautology} in Boolean logic (every cell in the output column of its truth table is $\true$), but $P \disj \neg P$ is \insist{not} a tautology in intuitionistic logic (some cells are $\true$ and some are not).

  If Boolean logic and intuitionistic logic disagree with each other, which one is ``correct''? As we've discussed in lecture, the most practical answer we can give is that \insist{both} systems of logic are \insist{useful} in the study of computation and the practice of crafting programs. As computer scientists and programmers, it is not our business to determine the absolute nature of the universe; it's our business to make things happen on computers!

  In particular, intuitionistic logic is useful when we want to reason about a computational setting where some problems may be \vocab{undecidable} (or \vocab{uncomputable}, which is a synonym in this setting). This is directly relevant to the study that we traditionally call ``theory of computation'', covered in courses like CS 311 and CS 581.

  \subsection{Truth values}

    \vocab{HT-logic} has \insist{three} truth values, which we traditionally call ``true'' (\true), ``false'' (\false), and ``not-false'' (\notfalse).

    Note that in HT logic, ``not-false'' is \insist{different} than ``true''! We sometimes call a value like $\notfalse$ an \vocab{intermediate-truth-value}: it is ``less true'' than $\true$, but ``more true'' than $\false$. A system of logic with at least one intermediate truth value is called an \vocab{intermediate-logic}, a term which encompasses intuitionistic logic and more.

    When we state that some expression $P$ has the truth value $\notfalse$, we are saying that we \insist{know} $P$ \insist{cannot be disproved}, but we are \insist{not} stating that we have actually \insist{proved} $P$. This turns out to be the actual reality of our knowledge about a variety of questions in mathematics and computer science.

    One way to describe the philosophical difference between \vocab{Boolean-logic} and \vocab{intuitionistic-logic} is that Boolean logic reasons about ``what is safe to assume'' and intuitionistic logic reasons about ``what we have already proved''. This description is a bit of a simplification, but it is helpful to keep in mind: if we know something \insist{cannot be disproved} but we have not \insist{proved} it, then it is something that is ``safe to assume'', but it is not something that we ``have already proved''.

    To be very precise, \insist{every individual system of logic defines its own concept of ``truth''}. The meaning of ``truth'' in Boolean logic is different than the meaning of ``truth'' in HT logic.

    \insist{There is no universal logical definition of ``truth''.} All we have are a bunch of different systems of logic, a history of logical practice suggesting which systems are useful for which tasks, and a history of philosophical arguments suggesting which systems align with which philosophical positions. This is the world of logic as we know it today.

  \subsection{Operators}

    We will start to see some patterns emerge here: our HT operator definitions are significantly different than our Boolean operator definitions, but there are some core properties that remain the same.

    \subsubsection{Negation}

      Here is the truth table in HT logic which defines \vocab{negation} (the ``not'' operator). Note that ``true'' and ``not-false'' behave the same under negation, but they behave differently under most of our other operators.

      \begin{center}
        \HTNEG
      \end{center}

      A consequence of this definition is that $\neg \neg P$ is \insist{different} than $P$ in HT logic:

      \begin{center}
        \begin{tabular}{c ? c ? c}
          $P$ & $\neg P$ & $\neg \neg P$ \\
          \thickhline
          \true & \false & \true \\
          \hline
          \notfalse & \false & \true \\
          \hline
          \false & \true & \false
        \end{tabular}
      \end{center}

      Again, note that this is in contrast to Boolean logic, where $P \equ \neg \neg P$ is a \vocab{tautology}.

      Negation in HT logic is a little strange compared to Boolean logic, but the idea is that $\neg \neg P$ is a \insist{weaker claim} than $P$. Specifically, $\neg \neg P$ says ``it is impossible to disprove $P$'', and $P$ says ``we have already proved $P$''.

    \subsubsection{Disjunction}

      Here is the truth table in HT logic which defines \vocab{disjunction} (the ``or'' operator). We will use the two-dimensional matrix style for two-input truth tables in HT logic, since it reveals an interesting pattern.

      \begin{center}
        \HTDISJ
      \end{center}

      Under the interpretation that $\true$ is ``more true`` than $\notfalse$, we might describe this truth table by saying that it outputs the ``most true'' out of its two inputs. Note that this \insist{also} describes the truth table for the $\disj$ operator in Boolean logic!

      In \insist{any} system of logic, when we have an operator that we call \vocab{disjunction} (or ``or''), it will nearly always have this property of choosing the ``most true'' out of its inputs. Boolean disjunction is actually just a special case of this principle.

    \subsubsection{Conjunction}

      Here is the truth table in HT logic which defines \vocab{conjunction} (the ``and'' operator).

      \begin{center}
        \HTCONJ
      \end{center}

      Notice the symmetry compared to the HT logic definition of \vocab{disjunction}: the $\conj$ operator always returns the ``least true'' of its two operands.

      In \insist{any} system of logic, when we have an operator that we call \vocab{conjunction} (or ``and''), it will nearly always have this property of choosing the ``least true'' out of its inputs. Boolean disjunction is actually just a special case of this principle.

    \subsubsection{Implication}

      Here is the truth table in HT logic which defines \vocab{implication} (the ``implies'' operator).

      \begin{center}
        \HTIMPL
      \end{center}

      This one is a little harder to recognize a pattern in, but the idea is that $P \impl Q$ corresponds to the claim ``$Q$ is at least as true as $P$''.

      This pattern might seem to fail in the $\notfalse$ output in the first row, but it is actually consistent with the general meaning of ``less than'' in \vocab{intuitionistic} logic, where it is possible for $x \less y$ to be false without $x \ge y$ being true. The $\notfalse$ output in the table corresponds to the idea that we can say $\true$ is ``not less true than'' $\notfalse$, but we cannot say it is ``at least as true as'' $\notfalse$; these are two \insist{different} things in intuitionistic logic.

      Putting aside the strange behavior of ``less than'' in intuitionistic logic, this pattern also applies to nearly any operator that we call \vocab{implication} in any system of logic. We will always have that $P \impl Q$ is ``true'' when $Q$ is ``at least as true'' as $P$, and we will always have that $P \impl Q$ is ``false'' when $Q$ is ``less true'' than $P$. Boolean implication is just a special case of this principle!

      This gives an explanation for the principle that ``false implies anything'': $\false$ is the ``least true'' truth value, so \insist{all} truth values are ``at least as true'' as $\false$. Again, this applies in nearly any system of logic with a truth value that we call ``false''.

    \subsubsection{Calculation}

      To calculate the value of an expression in HT logic, it will be most convenient to return to our other style of writing truth tables. For example:

      \begin{center}
        \begin{tabular}{c | c ? c ? c ? c ? c}
          $P$ & $Q$ & $\neg Q$ & $P \disj \neg Q$ & $\neg (P \disj \neg Q)$ & $\neg (P \disj \neg Q) \impl P$ \\
          \thickhline
          \true & \true & \false & \true & \false & \true \\
          \hline
          \true & \notfalse & \false & \true & \false & \true \\
          \hline
          \true & \false & \true & \true & \false & \true \\
          \hline
          \notfalse & \true & \false & \notfalse & \false & \true \\
          \hline
          \notfalse & \notfalse & \false & \notfalse & \false & \true \\
          \hline
          \notfalse & \false & \true & \true & \false & \true \\
          \hline
          \false & \true & \false & \false & \true & \false \\
          \hline
          \false & \notfalse & \false & \false & \true & \false \\
          \hline
          \false & \false & \true & \true & \false & \true \\
        \end{tabular}
      \end{center}

      Note that the process of calculation is exactly the same as in Boolean logic: we list the subexpressions and then look up each result in the truth table for the relevant operator. Again, this is actually the process of calculating with \vocabs{lookup-table} in general.



\section{Verilog}

  For one more example of a system of logic defined by truth tables, we will very briefly survey some of the logic of Verilog, a \vocab{hardware-description-language} for designing digital circuits.

  As software developers, we usually imagine that digital circuits are Boolean: modern processors present us with the ability to store and manipulate Boolean bits, so it is certainly at least correct to say that a processor is capable of \insist{simulating} Boolean calculations with extremely high accuracy.

  This is not the perspective of an electrical engineer, though! Digital circuits are fundamentally electrical circuits, which means that the input to a digital logic gate is actually an \insist{electrical} value: more like a wave than a bit, with two different measurements traditionally called \insist{voltage} and \insist{current}.

  \subsection{Truth values}

    In a standard digital circuit, electrical values are ``processed'' by measuring their voltage. The hardware system will have a built-in wire providing a constant high voltage signal, and another wire providing a constant low voltage signal. (The actual voltages of the high and low voltage wires vary between systems.)

    This is how electrical inputs are interpreted as bits: the voltage of the input signal is measured and compared to the high voltage wire and the low voltage wire. If it's closer to the high voltage value, it's a 1; if it's closer to the low voltage value, it's a 0.

    But this does not account for all possible electrical signals! In particular, there are at least two missing possibilities that designers of digital circuits care about:

    \begin{itemize}
      \item
        Two wires can be directly connected together, without any digital gates between them: the diagram might look like ${>}\!{-}$ with electricity flowing left-to-right. In this case, whichever input has the \insist{lower impedance} (higher current) will dominate the output signal.

        A digital circuit element can be set to output a ``high-impedance'' electrical value, which means that the value of its output wire will effectively be ``replaced'' by the value of any other wire connected to it. For example, this is the basis of how a bidirectional data bus works: while it's receiving data, it's ``outputting'' a high-impedence signal that gets ``replaced'' by the input signal.

      \item
        A signal might not have a stable voltage at all, or its voltage might be right in between ``low'' and ``high'', or it might just be way out of range compared to what the system expects of a meaningful digital signal. In other words, it might be ``nonsense''.

        A digital circuit element can be set to output a ``don't-care'' electrical value, which means that there is effectively no guarantee about its current or voltage (within electrically safe limits). For example, this is sometimes the kind of signal that hardware peripherals are set to ``output'' before being initialized by the operating system, like a graphics card or an optical disc drive.

        Within a system like Verilog, the practical purpose of the ``don't-care'' value is to provide safety checks, in order to make sure that the circuit designer is wiring things together in a way that will produce predictable behavior. If a ``don't-care'' value shows up \insist{unexpectedly} while analyzing a circuit, that almost certainly indicates a bug in the design of the circuit.
    \end{itemize}

    Digital circuit designers use these two intermediate electrical states in their everyday work, so the logic of Verilog defines \insist{four} truth values: ``true'' ($\true$ or $1$), ``false'' ($\false$ or $0$), ``high-impedance'' (\texttt{Z}), and ``don't-care'' (\texttt{X}).

  \subsection{Operators}

    Verilog includes many unique operators that are not found in Boolean or HT logic, but it also includes most of the standard operators that we've been discussing. Here are the truth tables that define \vocab{negation}, \vocab{conjunction}, and \vocab{disjunction} in the logic of Verilog.

    \begin{center}
      \VNEG \quad \VDISJ \quad \VCONJ
    \end{center}

    Although the interactions between $\hi$ and $\dc$ are a little strange, we can see that these tables are still consistent with the interpretation of $\true$ as the ``most true'' value and $\false$ as the ``most false'' value, with $\hi$ and $\dc$ as \vocab{intermediate-truth-values}.

    Like in intuitionistic logic, the meaning of ``less true than'' in Verilog is different than Boolean logic, which accounts for the behavior of $\hi$ and $\dc$ in the truth tables. In particular, $\hi$ and $\dc$ are simply ``incomparable'' as truth values: they are both between $\true$ and $\false$, but neither one is ``less true than'' the other.

    The idea behind all the $\dc$ cells is that in these cases, there is \insist{no guarantee} about the output value. It will most likely be the same as one or the other of the input values, but it might even be some totally random signal from somewhere else.

    This uncertainty applies when we have an input that we're uncertain about ($\dc$), but it also applies when we have an input that might be dominated by some other signal ($\hi$). In an abstract sense, we might consider $\hi$ to represent a signal whose value comes from ``outside the truth table'', so we can't reason about it \insist{within} the truth table except to say that it's uncertain.


\input{include/footer.tex}
\end{document}
