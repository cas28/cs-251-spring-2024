from __future__ import annotations
from dataclasses import dataclass

@dataclass(frozen=True) # immutable
class Node:
    head: int
    rest: LinkedList

type LinkedList = Node | None

# induction principle:
# if:
#   - base case: P(None)     # proof for empty list
#   - step case: assuming P(list), show ∀ head. P(Node(head, list))
#                P(list) → ∀(head ∈ int). P(Node(head, list))
# then:
#   ∀(list ∈ LinkedList). P(list)

# P(list) := (list.head == 1)
# P(list) := with_new_last(list, 1) == Node(2, Node(1, None))

# P(list) := reversed(reversed(list)) == list
# - base case: reversed(reversed(None)) == None
#     reversed(reversed(None))
#     == reversed(None)         by return value
#     == None                   by return value
# - step case: (reversed(reversed(rest)) == rest) →
#              (∀(head ∈ int). reversed(reversed(Node(head, rest)))
#                              == Node(head, list))

example1: LinkedList = None
example2: LinkedList = Node(1, None)
example3: LinkedList = Node(2, example2)

# struct A { int x; int y; int z; };
# void f(A a) { a.x = 1; }   // deep copy      entirely new mailbox
# void f(A *a) { a->x = 1; } // shallow copy   photocopying address
# void f(A *a) { a = nullptr; } 

def pretty_print(list: LinkedList):
    match list:
        case None:
            pass
        case Node(head, rest):
            print(head)
            pretty_print(rest)

def with_new_last(list: LinkedList, new_last: int) -> LinkedList:
    match list: # pattern matching
        case None:
            return Node(new_last, None)
        case Node(head, rest):
            return Node(head, with_new_last(rest, new_last))

# match m:
#   case 0: return n
#   case S(m): return S(m + n)

# S(S(0)) + S(S(S(0))) ==>
# S( S(0) + S(S(S(0))) ) ==>
# S(S( 0 + S(S(S(0))) )) ==>
# S(S( S(S(S(0))) ))

# Node(head, rest) unify with Node(1, None)
# yes, [ head := 1, rest := None ]

# with_new_last(Node(1, None), 2) ==>
# Node(1, with_new_last(None, 2)) ==>
# Node(1, Node(2, None))

# with_new_last(Node(1, (Node(2, None))), 3)

def reversed(list: LinkedList) -> LinkedList:
    match list:
        case None:
            return None
        case Node(head, rest):
            return concatenated(reversed(rest), Node(head, None))

# reversed(Node(1, Node(2, Node(3, None)))) ==>
#   [ head := 1, rest := Node(2, Node(3, None))]
# with_new_last(reversed(Node(2, Node(3, None))), 1) ==>
#   [ head := 2, rest := Node(3, None) ]
# with_new_last(with_new_last(reversed(Node(3, None)), 2), 1) ==>
#   [ head := 3, rest := None ]
# with_new_last(with_new_last(with_new_last(reversed(None), 3), 2), 1) ==>
# with_new_last(with_new_last(Node(3, None), 2), 1) ==>
#   [ head := 3, rest := None ]
# with_new_last(Node(3, with_new_last(None, 2)), 1) ==>
# with_new_last(Node(3, Node(2, None)), 1) ==>
#   [ head := 3, rest := Node(2, None) ]
# Node(3, with_new_last(Node(2, None), 1)) ==>
#   [ head := 2, rest := None, new_last := 1 ]
# Node(3, Node(2, with_new_last(None, 1))) ==>
# Node(3, Node(2, Node(1, None)))

def concatenated(list1: LinkedList, list2: LinkedList) -> LinkedList:
    match list1:
        case None:
            return list2
        case Node(head1, rest1):
            return Node(head1, concatenated(rest1, list2))

# ∀ list. concatenated(None, list) == list
# concatenated(None, list) == list    by definition of concatenated


# P(list) := (concatenated(list, None) == list)

# ∀ list. concatenated(list, None) == list

# base: concatenated(None, None) == None
# by definition of concatenated

# step: (concatenated(list, None) == list) ->
#       ∀ head. (concatenated(Node(head, list), None) == Node(head, list))

# concatenated(Node(head, list), None) ==>           by definition of concatenated
#   [ head1 := head, rest1 := list, list2 := None ]
# Node(head, concatenated(list, None)) ==>
# Node(head, list)                                   by inductive hypothesis
