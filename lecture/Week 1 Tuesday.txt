bool not(bool p) {
  switch (p) {
    case true: return false;
    case false: return true;
  }
}

bool and(bool p, bool q) {
  switch (p) {
    case true:
      switch (q) {
        case true: return true;
        case false: return false;
      }
    case false:
      switch (q) {
        case true: return false;
        case false: return false;
      }
  }
}

bool implies(bool p, bool q) {
  switch (p) {
    case true:
      switch (q) {
        case true: return true;
        case false: return false;
      }
    case false:
      switch (q) {
        case true: return true;
        case false: return true;
      }
  }
}

-----------------------------------------------

e: even
o: odd

 x | y || x + y
----------------
 e | e || e
 e | o || o
 o | e || o
 o | o || e
